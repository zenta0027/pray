﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    float time = 3.0f;
    float prevTime;
    bool playerActivated = false;
    bool gameOver = false;
    GameObject countText;
    public GameObject count_1;
    public GameObject count_2;
    public GameObject count_3;
    public GameObject soundManager;
    public GameObject platforms;
    public GameObject lava;
    public GameObject[] winTexts;
    public GameObject restartText;
    bool anyoneDie = false;
    bool anyoneNearWin = false;

    // Use this for initialization
    void Start()
    {
        count_3.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        if (!playerActivated)
        {
            prevTime = time;
            time -= Time.deltaTime;
            if (time <= 2.0f && prevTime > 2.0f)
            {
                count_3.SetActive(false);
                count_2.SetActive(true);
            }
            else if (time <= 1.0f && prevTime > 1.0f)
            {
                count_2.SetActive(false);
                count_1.SetActive(true);
            }
            else if (time <= 0.0f && prevTime > 0.0f)
            {
                count_1.SetActive(false);
                platforms.GetComponent<WheelScript>().activatePlayers();
                soundManager.GetComponent<SoundManager>().playIntro1();
                lava.GetComponent<LavaScript>().activateLava();
                playerActivated = true;
            }
        }
        else if(gameOver)
        {
            float readyAxis = Input.GetAxis("P1_Ready") + Input.GetAxis("P2_Ready") + Input.GetAxis("P3_Ready") + Input.GetAxis("P4_Ready");
            if (readyAxis > 0.0f)
            {
                Application.LoadLevel("MainMenuScene");
            }
        }
    }

    public void showWinner(int playerNum)
    {
        if(!gameOver)
        {
            restartText.SetActive(true);
            winTexts[playerNum - 1].SetActive(true);
            gameOver = true;
        }
    }

    public void playerKilledByPray()
    {
        if (!anyoneDie && !anyoneNearWin)
        {
            anyoneDie = true;
            Debug.Log("Let the party begin!");
            soundManager.GetComponent<SoundManager>().playIntro2();
        }
    }

    public void twoPlayerDiedOnSameAltar()
    {
        if(!anyoneNearWin)
        {
            anyoneNearWin = true;
            Debug.Log("The end is coming!");
            soundManager.GetComponent<SoundManager>().playIntro3();
        }
    }
}
