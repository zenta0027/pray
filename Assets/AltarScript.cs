﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class AltarScript : MonoBehaviour
{
    public int floorNum;
    public bool disableOwnerOnStart = true;

    float weightSum = 0;
    List<GameObject> playersOnFloor;
    int altarLightOn = 1;
    GameObject owner;
    GameObject whitenSprite;
    public GameObject Explosion;
    public GameObject gameController;
    Vector2 center;
	// Use this for initialization
	void Start ()
    {
        Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();

        playersOnFloor = new List<GameObject>();
        // int platformNumber = (int)this.gameObject.name[gameObject.name.Length - 1];
        for(int i = 1; i < 4; i++)
        {
            GameObject g = transform.Find("altarLight_" + floorNum + "_" + i).gameObject;
            g.GetComponent<Renderer>().enabled = false;
        }
        owner = GameObject.Find("player" + floorNum);
        owner.SetActive(!disableOwnerOnStart);
        whitenSprite = transform.Find("altar_white").gameObject;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();

        float prayProgress = owner.GetComponent<PlayerController>().getPrayProgress();
        whitenSprite.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, prayProgress);
    }

    void OnDisable()
    {
        if (playersOnFloor == null)
            return;

        if (gameObject == null)
            return;

        List<GameObject> copiedPlayersOnFloor = new List<GameObject>();

        foreach (GameObject player in playersOnFloor)
        {
            copiedPlayersOnFloor.Add(player);
        }

        foreach (GameObject player in copiedPlayersOnFloor)
        {
            if(player != null)
                player.GetComponent<PlayerController>().tryToLeaveFromSpecificFloor(gameObject);
        }
    }

    public void addWeight(float weight)
    {
        weightSum = weightSum + weight;
    }

    public void removeWeight(float weight)
    {
        weightSum = weightSum - weight;
    }

    public float getAngularPower(float angle)
    {
        return weightSum * -Mathf.Cos(angle);
    }

    public void addGameObejct(GameObject o)
    {
        if (!playersOnFloor.Contains(o))
            playersOnFloor.Add(o);
    }

    public void removeGameObject(GameObject o)
    {
        if(playersOnFloor.Contains(o))
            playersOnFloor.Remove(o);
    }

    public void killPlayers()
    {
        List<GameObject> copiedPlayersOnFloor = new List<GameObject>();
        List<GameObject> killingList = new List<GameObject>();
        GameObject copiedObject;

        foreach (GameObject player in playersOnFloor)
        {
            copiedPlayersOnFloor.Add(player);
        }
        
        foreach (GameObject player in copiedPlayersOnFloor)
        {
            PlayerController playerController = player.GetComponent<PlayerController>();
            if (playerController.playerNum != floorNum && !playerController.isInvulnerable())
            {
                Debug.Log("죽어라 플레이어 " + playerController.playerNum);
                playerController.createDeadEffect(1.0f);
                copiedObject = Instantiate(Explosion);
                copiedObject.transform.position = player.transform.position;
                copiedObject.SetActive(true);
                playerController.kill(true);
                killingList.Add(player);
                if (altarLightOn <= 3)
                {
                    transform.Find("altarLight_" + floorNum + "_" + altarLightOn).gameObject.GetComponent<Renderer>().enabled = true;
                    altarLightOn++;
                }
                if(altarLightOn == 3 && gameController != null)
                {
                    gameController.GetComponent<GameController>().twoPlayerDiedOnSameAltar();
                }
                if(altarLightOn == 4 && gameController != null)
                {
                    gameController.GetComponent<GameController>().showWinner(floorNum);
                }
            }
        }
        foreach(GameObject player in killingList)
        {
            removeGameObject(player);
        }
    }

    public void activatePlayer()
    {
        owner.SetActive(true);
    }

    public GameObject getOwner()
    {
        return owner;
    }

    public int getAlterLightOn()
    {
        return altarLightOn;
    }
}
