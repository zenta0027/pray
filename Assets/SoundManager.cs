﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioSource intro1;
    public AudioSource intro2;
    public AudioSource intro3;
    public AudioSource loop1;
    public AudioSource loop2;
    public AudioSource loop3;

    enum CurrentPlay {none, play_intro1, play_intro2, play_intro3, play_loop1, play_loop2, play_loop3};
    CurrentPlay currentPlay = CurrentPlay.none;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if(currentPlay == CurrentPlay.play_intro1 && !intro1.isPlaying)
        {
            currentPlay = CurrentPlay.play_loop1;
            loop1.loop = true;
            loop1.Play();
        }
        else if(currentPlay == CurrentPlay.play_intro2 && !intro2.isPlaying)
        {
            currentPlay = CurrentPlay.play_loop2;
            loop2.loop = true;
            loop2.Play();
        }
        else if(currentPlay == CurrentPlay.play_intro3 && !intro3.isPlaying)
        {
            currentPlay = CurrentPlay.play_loop3;
            loop3.loop = true;
            loop3.Play();
        }
	}

    public void playIntro1()
    {
        intro1.Play();
        currentPlay = CurrentPlay.play_intro1;
    }

    public void playIntro2()
    {
        currentPlay = CurrentPlay.none;
        if (intro1.isPlaying) intro1.Stop();
        if (loop1.isPlaying) loop1.Stop();
        intro2.Play();
        currentPlay = CurrentPlay.play_intro2;
    }

    public void playIntro3()
    {
        currentPlay = CurrentPlay.none;
        if (intro2.isPlaying) intro2.Stop();
        if (loop2.isPlaying) loop2.Stop();
        intro3.Play();
        currentPlay = CurrentPlay.play_intro3;
    }
}
