﻿using UnityEngine;
using System.Collections;

public class SkullScript : MonoBehaviour
{

    int boundCount = 3;
    float gravity = 40.0f;
    float minSpeedYOnAir = -40.0f;
    float minSpeedYInLava = -2.0f;
    float angularVelOnAir = 800.0f;
    float angularVelInLava = 100.0f;
    float angluarVelMul = 0.0f;
    float boundness = 0.7f;
    float speedXMulInLava = 0.1f;
    bool isInLava = false;
    public GameObject lavaBorder;

    // Use this for initialization
    void Start()
    {
    }

    public void Init(float explosivePower, float angle)
    {
        angluarVelMul = Random.Range(-1.0f, 1.0f);
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(explosivePower * Mathf.Cos(angle), explosivePower * Mathf.Sin(angle));
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();
        float speedX = body.velocity.x;
        float speedY = body.velocity.y;

        float minSpeedY = minSpeedYOnAir;
        float angularVel = angularVelOnAir;
        if (gameObject.GetComponent<Collider2D>().bounds.min.y <= lavaBorder.GetComponent<SpriteRenderer>().bounds.max.y)
        {
            minSpeedY = minSpeedYInLava;
            angularVel = angularVelInLava;
            if (!isInLava)
            {
                speedX = speedX * speedXMulInLava;
                isInLava = true;
            }
        }

        speedY = Mathf.Max(speedY - gravity * Time.deltaTime, minSpeedY);
        body.velocity = new Vector2(speedX, speedY);
        body.angularVelocity = angularVel * angluarVelMul;

        if (gameObject.GetComponent<Collider2D>().bounds.max.y <= -10.0f)
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Platform" && boundCount > 0)
        {
            boundCount = boundCount - 1;
            Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();
            float speedX = body.velocity.x;
            float speedY = body.velocity.y;
            body.velocity = new Vector2(speedX, -speedY * boundness);
        }
    }
}
