﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {

    public float fadeTime = 0.0f;
    public float currentTime = 0.0f;

	// Use this for initialization
	void Start () {
        currentTime = fadeTime;
    }
	
	// Update is called once per frame
	void Update () {
        currentTime -= Time.deltaTime;
        float pivot = 1.0f;
        if(fadeTime > 0.0f)
            pivot = Mathf.Min(1.0f, Mathf.Max(0.0f, currentTime / fadeTime));
        GetComponent<SpriteRenderer>().color = new Color(pivot, pivot, pivot, pivot);
        if(currentTime <= 0.0f)
            Destroy(gameObject);
	}
}
