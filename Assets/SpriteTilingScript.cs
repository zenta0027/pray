﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]

public class SpriteTilingScript : MonoBehaviour
{

    public Vector2 repeatPivot;
    SpriteRenderer sprite;
    // Use this for initialization
    void Start()
    {
        float screenWidth = Screen.width;
        // Tiling lava
        sprite = GetComponent<SpriteRenderer>();
        Vector2 spriteSize_wu = new Vector2(sprite.bounds.size.x / transform.localScale.x, sprite.bounds.size.y / transform.localScale.y);

        GameObject repeatingObject = transform.GetChild(0).gameObject;
        Vector3 scale = repeatingObject.transform.localScale;

        SpriteRenderer repeatingSprite = repeatingObject.GetComponent<SpriteRenderer>();
        Vector3 repeatingSpriteSize = new Vector3(repeatingSprite.bounds.size.x / transform.localScale.x * repeatPivot.x, repeatingSprite.bounds.size.y / transform.localScale.y * repeatPivot.y, 0);
        Vector3 pivot = new Vector3(sprite.bounds.size.x / 2, sprite.bounds.size.y / 2, 0);

        int h = Mathf.RoundToInt(sprite.bounds.size.y);
        int w = Mathf.RoundToInt(sprite.bounds.size.x);

        for (int i = 0; i * spriteSize_wu.y * scale.x < h; i++)
        {
            for (int j = 0; j * spriteSize_wu.x * scale.y < w; j++)
            {
                GameObject child = Instantiate(repeatingObject);
                child.GetComponent<SpriteRenderer>().enabled = true;
                child.transform.position = transform.position + pivot - (new Vector3(spriteSize_wu.x * scale.x * j, spriteSize_wu.y * scale.y * i, 0)) + repeatingSpriteSize;
                child.transform.localScale = scale;
                child.transform.parent = transform;
            }
        }

        Destroy(repeatingObject);
        sprite.enabled = false;

        /*
        //Lava Border
        lavaBorder = transform.Find("lavaBorder").gameObject;
        
        for(int i = 0; i*spriteSize_wu.x < w; i++)
        {
            GameObject child = Instantiate(lavaBorder);
            child.transform.position = pivot - (new Vector3(spriteSize_wu.x * i, 0, -1.5f));
            child.transform.localScale = scale * 5;
            child.transform.parent = transform;
        }
        */

    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (lavaBorder.GetComponent<SpriteRenderer>().bounds.max.y < maxHeight)
        {
            gameObject.transform.position += new Vector3(0.0f, lavaSpeed * Time.deltaTime, 0.0f);
        }*/
        //Change the height of lava here.

    }
}
