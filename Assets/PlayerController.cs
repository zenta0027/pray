﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    #region Sound
    public AudioSource PraySound;
    public AudioSource KilledSound;
    public AudioSource JumpSound;
    public AudioSource LavaSound;
    public GameObject gameController;
    #endregion

    Animator playerAnimation;
    public int playerNum;

    const float gravity = 40.0f;
    const float minSpeedY = -40.0f;
    
    public GameObject playerFloor;

    #region Time
    const float prayCastTime = 0.25f;
    const float prayCastDelay = 0.75f;
    const float reviveTime = 5.0f;
    const float stunTime = 2.5f;
    const float invulnerableTimeMax = 3.0f;
    float invulnerableTime;
    float delayTime;
    #endregion

    bool isOnAir = true;
    Vector2 oldPosition;
    GameObject lastSteppedFloor;
    int extraJumpCount;
    int weight;
    bool jumpKeyPressed;
    bool liftKeyPressed;
    enum unitState { normal, stunned, pray, prayDelay, dead, lift, lifted };
    unitState state;
    Vector3 playerLookingDirection;
    List<GameObject> contactPlayers;
    GameObject liftPlayer;
    bool throwable;

    #region Animation Objects
    public GameObject lavaBorder;
    public GameObject skullHead;
    public GameObject skullBody;
    public GameObject skullLeg;
    public GameObject arrow;
    #endregion

    float cameraHeight;
    Vector2 oldVelocity;

    // Use this for initialization
    void Start()
    {
        contactPlayers = new List<GameObject>();
        gameObject.GetComponent<SpriteRenderer>().enabled = true;

        moveToFloor();
        extraJumpCount = 1;
        weight = 1;
        jumpKeyPressed = false;
        liftKeyPressed = false;
        state = unitState.normal;
        invulnerableTime = 0;
        cameraHeight = Camera.main.orthographicSize;
        playerAnimation = GetComponent<Animator>();
        playerAnimation.SetBool("playerWalking", false);
        playerAnimation.SetBool("playerPraying", false);
        playerAnimation.SetBool("playerLifted", false);
        playerLookingDirection = transform.localScale;
        oldVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
    }

    // Update is called once per frame
    void Update()
    {
        float jumpAxis = Input.GetAxisRaw("P" + playerNum + "_Jump");
        float liftAxis = Input.GetAxis("P" + playerNum + "_Lift");
        bool jumpKeyPressedOnThisFrame = false;
        bool liftKeyPressedOnThisFrame = false;
        Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();
        float speedX = 0.0f;
        float speedY = body.velocity.y;

        #region JUMP KEY INPUT
        if (jumpAxis > 0.5f && !jumpKeyPressed)
        {
            jumpKeyPressed = true;
            jumpKeyPressedOnThisFrame = true;
        }
        else if (jumpAxis < 0.5f && jumpKeyPressed)
        {
            jumpKeyPressed = false;
        }
        #endregion
        #region LIFT KEY INPUT
        if (liftAxis > 0.2f && !liftKeyPressed)
        {
            liftKeyPressed = true;
            liftKeyPressedOnThisFrame = true;
        }
        else if (liftAxis < 0.2f && liftKeyPressed)
        {
            liftKeyPressed = false;
        }
        #endregion

        if (state != unitState.dead)
        {
            if (lastSteppedFloor != null)
            {
                Rigidbody2D lastSteppedFloorBody = lastSteppedFloor.GetComponent<Rigidbody2D>();
                speedX = lastSteppedFloorBody.velocity.x;
                speedY = Mathf.Max(lastSteppedFloorBody.velocity.y, speedY - gravity * Time.deltaTime, minSpeedY);
            }
            else
            {
                speedY = Mathf.Max(speedY - gravity * Time.deltaTime, minSpeedY);
            }
        }
        playerAnimation.SetBool("playerWalking", false);


        #region INVULNERABLE PROCESS
        if (invulnerableTime > 0.0f)
        {
            if ((int)(invulnerableTime * 5) % 2 == 0)
                GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            else
                GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
        }
        invulnerableTime = invulnerableTime - Time.deltaTime;
        if (invulnerableTime <= 0.0f)
            GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        #endregion

        //Move unit only when it is neither stunned nor dead.
        if (this.state == unitState.normal || this.state == unitState.lift)
        {
            #region Press Pray Button
            float prayAxis = Input.GetAxis("P" + playerNum + "_Pray");
            if (!isOnAir && prayAxis > 0.2f && state == unitState.normal)
            {
                PraySound.PlayOneShot(PraySound.clip, PraySound.volume);
                Debug.Log("Praying start!");
                playerAnimation.SetBool("playerPraying", true);
                this.state = unitState.pray;
                delayTime = prayCastTime;
            }
            #endregion

            else if (liftKeyPressedOnThisFrame)
            {
                if (this.state == unitState.normal)
                {
                    #region Lift Stunned Player
                    GameObject contactPlayer = null;
                    float distance = 10000.0f;
                    foreach (GameObject player in contactPlayers)
                    {
                        if (player.GetComponent<PlayerController>().state == unitState.stunned)
                        {
                            if (distance > (player.GetComponent<Rigidbody2D>().position - body.position).magnitude)
                            {
                                contactPlayer = player;
                                distance = (player.GetComponent<Rigidbody2D>().position - body.position).magnitude;
                            }
                        }
                    }
                    if (contactPlayer != null && !isOnAir && !contactPlayer.GetComponent<PlayerController>().isOnAir)
                    {
                        Debug.Log("I got you in my sights, player " + contactPlayer.GetComponent<PlayerController>().playerNum);
                        liftPlayer = contactPlayer;
                        liftPlayer.GetComponent<PlayerController>().state = unitState.lifted;
                        liftPlayer.GetComponent<PlayerController>().playerAnimation.SetBool("playerLifted", true);
                        liftPlayer.GetComponent<PlayerController>().liftPlayer = gameObject;
                        liftPlayer.GetComponent<Rigidbody2D>().position = gameObject.GetComponent<Rigidbody2D>().position + new Vector2(0.0f, 1.0f);
                        changeWeight(2);
                        state = unitState.lift;
                    }
                    #endregion
                }
                else
                {
                    #region Throw Player
                    Debug.Log("HaHaHa, Goodbye");
                    if (playerLookingDirection.x > 0)
                    {
                        liftPlayer.GetComponent<Rigidbody2D>().velocity += new Vector2(5.0f, 0.0f);
                    }
                    else
                    {
                        liftPlayer.GetComponent<Rigidbody2D>().velocity += new Vector2(-5.0f, 0.0f);
                    }
                    liftPlayer.GetComponent<PlayerController>().state = unitState.stunned;
                    liftPlayer.GetComponent<PlayerController>().liftPlayer = null;
                    liftPlayer.GetComponent<PlayerController>().changeWeight(1);
                    liftPlayer = null;
                    changeWeight(1);
                    state = unitState.normal;
                    #endregion
                }
            }
            else
            {
                #region Move & Jump
                float horizontalAxis = Input.GetAxis("P" + playerNum + "_Horizontal");
                speedX += 5.0f * horizontalAxis;

                if (horizontalAxis > 0.0f)
                {
                    playerAnimation.SetBool("playerWalking", true);
                    playerLookingDirection.x = Mathf.Abs(playerLookingDirection.x);
                    transform.localScale = playerLookingDirection;
                }
                else if (horizontalAxis < -0.0f)
                {
                    playerAnimation.SetBool("playerWalking", true);
                    playerLookingDirection.x = -Mathf.Abs(playerLookingDirection.x);
                    transform.localScale = playerLookingDirection;
                }
                else
                {
                    playerAnimation.SetBool("playerWalking", false);
                }

                if (!isOnAir)
                {
                    if (jumpKeyPressedOnThisFrame)
                    {
                        speedY = 20.0f;
                        playerAnimation.SetBool("playerWalking", false);
                    }
                }
                else
                {
                    if (jumpKeyPressedOnThisFrame && extraJumpCount > 0)
                    {
                        if (speedY < 15.0f) //This is for adjust air-jump timing. If we need, you can delete this if phrase.
                        {
                            speedY = 20.0f;
                            playerAnimation.SetBool("playerWalking", false);
                            extraJumpCount--; // In any case, you should lose all your chance of jump after air-jump.
                        }
                    }
                }
                #endregion
            }
        }
        else
        {
            #region Check DelayTime
            delayTime -= Time.deltaTime;
            if (delayTime <= 0)
            {
                if (this.state == unitState.stunned && isOnAir == false)
                {
                    this.state = unitState.normal;
                    playerAnimation.SetBool("playerLifted", false);
                    playerAnimation.SetBool("playerWalking", false);
                }
                else if (this.state == unitState.prayDelay)
                {
                    this.state = unitState.normal;
                    playerAnimation.SetBool("playerPraying", false);
                }
                else if (this.state == unitState.pray)
                {
                    Debug.Log("신의 죽창을 받아라!");
                    this.playerFloor.GetComponent<AltarScript>().killPlayers();
                    this.state = unitState.prayDelay;
                    delayTime += prayCastDelay;
                }
                else if (this.state == unitState.dead && playerFloor.activeSelf)
                {
                    Debug.Log("아아 살아난다아아");
                    this.revive();
                }
                else if (state == unitState.lifted)
                {
                    playerAnimation.SetBool("playerLifted", false);
                    playerAnimation.SetBool("playerWalking", false);
                    state = unitState.normal;
                    liftPlayer.GetComponent<PlayerController>().state = unitState.normal;
                    liftPlayer.GetComponent<PlayerController>().liftPlayer = null;
                    liftPlayer.GetComponent<PlayerController>().changeWeight(1);
                    liftPlayer = null;
                    changeWeight(1);
                }
            }
            #endregion
            if (state == unitState.stunned && isOnAir)
            {
                speedX = body.velocity.x;
            }
        }

        if (state != unitState.lifted)
        {
            body.velocity = new Vector2(speedX, speedY);
        }

        if (state == unitState.lift)
        {
            liftPlayer.GetComponent<PlayerController>().playerLookingDirection.x = playerLookingDirection.x;
            liftPlayer.GetComponent<Transform>().localScale = playerLookingDirection;
            liftPlayer.GetComponent<Rigidbody2D>().velocity = body.velocity;
        }
        else
        {
        }

        oldPosition = body.position;
        if (this.state != unitState.dead && ((gameObject.GetComponent<Collider2D>().bounds.min.y <= lavaBorder.GetComponent<SpriteRenderer>().bounds.max.y && invulnerableTime <= 0.0f) || gameObject.GetComponent<Collider2D>().bounds.max.y <= -10.0f))
        {
            createDeadEffect(0.2f);
            kill(false);
        }
        float playerPosition = gameObject.GetComponent<Collider2D>().bounds.min.y;
        arrow.transform.position = new Vector3(gameObject.transform.position.x, cameraHeight - 0.7f - (playerPosition - cameraHeight) * 0.1f, playerPosition);
        if (playerPosition > cameraHeight && state != unitState.dead) arrow.GetComponent<SpriteRenderer>().enabled = true;
        else arrow.GetComponent<SpriteRenderer>().enabled = false;

        oldVelocity = body.velocity;
    }

    void OnTriggerContact(Collider2D other)
    {
        if (state != unitState.dead && state != unitState.lifted)
        {
            if (other.tag == "Player")
            {
                unitState otherState = other.GetComponent<PlayerController>().state;
                if (otherState != unitState.stunned && otherState != unitState.lifted && otherState != unitState.dead)
                {
                    Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();
                    Collider2D collider = gameObject.GetComponent<Collider2D>();

                    GameObject otherPlayer = other.gameObject;
                    Rigidbody2D otherBody = otherPlayer.GetComponent<Rigidbody2D>();
                    float otherPlayerLowerY = otherPlayer.GetComponent<PlayerController>().oldPosition.y - other.bounds.extents.y;
                    float otherPlayerUpperY = otherPlayer.GetComponent<PlayerController>().oldPosition.y + other.bounds.extents.y;
                    float characterLowerY = oldPosition.y - collider.bounds.extents.y;
                    //if (characterLowerY >= otherPlayerUpperY && body.velocity.y <= otherBody.velocity.y)
                    if (characterLowerY >= otherPlayerUpperY && oldVelocity.y <= otherBody.velocity.y)
                    {
                        Debug.Log("Gotcha");
                        body.velocity = new Vector2(body.velocity.x, otherBody.velocity.y + 10.0f);
                        otherPlayer.GetComponent<PlayerController>().changestateStunned();
                        JumpSound.PlayOneShot(JumpSound.clip, JumpSound.volume);
                    }
                }
            }
            else if (other.gameObject.GetComponent<AltarScript>() != null)
            {
                Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();
                Collider2D collider = gameObject.GetComponent<Collider2D>();

                GameObject platform = other.gameObject;
                float platformLowerY = other.bounds.min.y;
                float platformUpperY = other.bounds.max.y;
                float charcterLowerY = oldPosition.y - collider.bounds.extents.y;

                if (charcterLowerY >= platformLowerY && body.velocity.y <= platform.GetComponent<Rigidbody2D>().velocity.y)
                {
                    body.position = new Vector2(body.position.x, platformUpperY + collider.bounds.extents.y);
                    if (state == unitState.lift)
                    {
                        liftPlayer.GetComponent<Rigidbody2D>().position = body.position + new Vector2(0.0f, 1.0f);
                    }

                    contactWithNewFloor(platform);
                    isOnAir = false;
                    extraJumpCount = 1;
                }
            }
        }
    }

    void changestateStunned()
    {
        //무적이면 스턴에 면역
        if (invulnerableTime > 0.0f)
            return;

        if(this.state == unitState.lift)
        {
            changeWeight(1);
        }
        if(state != unitState.lifted)
        {
            delayTime = stunTime;
        }
        this.state = unitState.stunned;
        playerAnimation.SetBool("playerWalking", false);
        playerAnimation.SetBool("playerPraying", false);
        playerAnimation.SetBool("playerLifted", true);
        if (liftPlayer != null)
        {
            PlayerController targetPlayer = liftPlayer.GetComponent<PlayerController>();
            targetPlayer.liftPlayer = null;
            liftPlayer = null;
            targetPlayer.changestateStunned();
            
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        OnTriggerContact(other);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        OnTriggerContact(other);
        if (other.tag == "Player")
        {
            Debug.Log("Hello!");
            contactPlayers.Add(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("GoodBye!");
            contactPlayers.Remove(other.gameObject);
        }
        else if (state != unitState.dead && other.gameObject.GetComponent<AltarScript>() != null)
        {
            GameObject platform = other.gameObject;
            if (lastSteppedFloor == platform)
            {
                //If Player leaved from platform, net weight on the floor shoul be reduced
                tryToLeaveFromLastSteppedFloor();
                isOnAir = true;
            }
        }
    }

    public void kill(bool killedByPray)
    {
        foreach(GameObject player in contactPlayers)
        {
            player.GetComponent<PlayerController>().contactPlayers.Remove(gameObject);
        }
        contactPlayers = new List<GameObject>();
        if (killedByPray == true && gameController != null)
        {
            KilledSound.PlayOneShot(KilledSound.clip, KilledSound.volume);
            gameController.GetComponent<GameController>().playerKilledByPray();
        }
        else
            LavaSound.PlayOneShot(LavaSound.clip, LavaSound.volume);

        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        arrow.GetComponent<SpriteRenderer>().enabled = false;
        this.state = unitState.dead;
        delayTime = reviveTime;
        playerAnimation.SetBool("playerWalking", false);
        playerAnimation.SetBool("playerPraying", false);
        playerAnimation.SetBool("playerLifted", false);
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        gameObject.GetComponent<Rigidbody2D>().position = new Vector2(0, 20);
        if (liftPlayer != null)
        {
            liftPlayer.GetComponent<PlayerController>().state = unitState.stunned;
            liftPlayer.GetComponent<PlayerController>().liftPlayer = null;
            liftPlayer.GetComponent<PlayerController>().changeWeight(1);
            liftPlayer = null;
            changeWeight(1);
        }
        tryToLeaveFromLastSteppedFloor();
    }

    public void revive()
    {
        isOnAir = true;
        if (lastSteppedFloor != null)
            Debug.LogError("밟고 있는 발판이 있다고 하는데 업성야함");
        gameObject.GetComponent<Collider2D>().enabled = true;
        moveToFloor();
        extraJumpCount = 1;
        jumpKeyPressed = false;
        playerAnimation.SetBool("playerWalking", false);
        playerAnimation.SetBool("playerPraying", false);
        playerAnimation.SetBool("playerLifted", false);
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        liftPlayer = null;
        changeWeight(1);
        invulnerableTime = invulnerableTimeMax;
        state = unitState.normal;
    }

    public void createDeadEffect(float powerValue)
    {
        Rigidbody2D body = gameObject.GetComponent<Rigidbody2D>();
        Vector3 pos = body.position;
        GameObject copiedObj;

        float randomAngle = Random.Range(Mathf.PI * 1 / 3, Mathf.PI * 2 / 3);

        copiedObj = Instantiate(skullHead);
        copiedObj.SetActive(true);
        copiedObj.GetComponent<Rigidbody2D>().position = pos + new Vector3(0.0f, 0.3f, 0.0f);
        copiedObj.GetComponent<SkullScript>().Init(15.0f * powerValue, randomAngle);

        copiedObj = Instantiate(skullBody);
        copiedObj.SetActive(true);
        copiedObj.GetComponent<Rigidbody2D>().position = pos + new Vector3(0.0f, -0.1f, 0.0f);
        copiedObj.GetComponent<SkullScript>().Init(14.0f * powerValue, Random.Range(Mathf.PI * 1 / 6, randomAngle));

        copiedObj = Instantiate(skullLeg);
        copiedObj.SetActive(true);
        copiedObj.GetComponent<Rigidbody2D>().position = pos + new Vector3(0.0f, -0.4f, 0.0f);
        copiedObj.GetComponent<SkullScript>().Init(17.0f * powerValue, Random.Range(randomAngle, Mathf.PI * 5 / 6));
    }

    public void tryToLeaveFromSpecificFloor(GameObject floor)
    {
        if (lastSteppedFloor == floor)
        {
            tryToLeaveFromLastSteppedFloor();
        }
    }

    void tryToLeaveFromLastSteppedFloor()
    {
        if (lastSteppedFloor != null)
        {
            //If Player leaved from platform, net weight on the floor shoul be reduced
            AltarScript floorData = lastSteppedFloor.GetComponent<AltarScript>();
            floorData.removeWeight(weight);
            lastSteppedFloor = null;
            floorData.removeGameObject(gameObject);
        }
    }

    void contactWithNewFloor(GameObject floor)
    {
        tryToLeaveFromLastSteppedFloor();

        lastSteppedFloor = floor;
        AltarScript floorData = lastSteppedFloor.GetComponent<AltarScript>();
        floorData.addWeight(weight);
        floorData.addGameObejct(gameObject);
    }

    void changeWeight(int newWeight)
    {
        if (weight == newWeight) return;

        GameObject currentSteppedFloor = lastSteppedFloor;
        tryToLeaveFromLastSteppedFloor();

        weight = newWeight;

        if (currentSteppedFloor != null)
            contactWithNewFloor(currentSteppedFloor);
    }
    
    public float getPrayProgress()
    {
        if (state == unitState.pray)
        {
            return 1 - delayTime / prayCastTime;
        }

        return 0.0f;
    }

    public void moveToFloor()
    {
        BoxCollider2D floorBox = playerFloor.GetComponent<BoxCollider2D>();
        BoxCollider2D playerBox = gameObject.GetComponent<BoxCollider2D>();
        Rigidbody2D floorBody = playerFloor.GetComponent<Rigidbody2D>();

        gameObject.GetComponent<Rigidbody2D>().position = new Vector2(floorBody.position.x, floorBox.bounds.max.y + playerBox.size.y * 1.8f + playerBox.offset.y);
        transform.position = gameObject.GetComponent<Rigidbody2D>().position;
        //gameObject.GetComponent<Rigidbody2D>().MovePosition(new Vector2(floorBody.position.x, floorBox.bounds.max.y + playerBox.size.y + playerBox.offset.y + 0.1f));
        oldPosition = gameObject.GetComponent<Rigidbody2D>().position;
    }

    public bool isAlive()
    {
        return state != unitState.dead;
    }

    public bool isInvulnerable()
    {
        return invulnerableTime > 0.0f;
    }
}
