﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class WheelScript : MonoBehaviour
{
    public float deltaVelocityPerWeight;
    public float maxAngluarVelocity;
    public float anglurVelocityDecrease;
    public GameObject soundManager;
    List<AltarScript> floors;
    float angularVelocity;
    float time = 3.0f;
    float prevTime;
    bool playerActivated = false;
    GameObject countText;
    public GameObject count_1;
    public GameObject count_2;
    public GameObject count_3;
    float angle = 0.0f;
    public float radius;

    // Use this for initialization
    void Start()
    {
        angle = Random.Range(0.0f, 2 * Mathf.PI);
        floors = new List<AltarScript>();
        Vector2 center = gameObject.transform.position;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            AltarScript floor = gameObject.transform.GetChild(i).GetComponent<AltarScript>();
            floors.Add(floor);

            Rigidbody2D body = floor.GetComponent<Rigidbody2D>();
            float floorAngle = angle + (float)floor.floorNum * Mathf.PI * 0.5f;
            body.MovePosition(center + new Vector2(radius * Mathf.Cos(floorAngle), radius * Mathf.Sin(floorAngle)));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(playerActivated)
        {
            float totalWeight = 0.0f;
            foreach (AltarScript floor in floors)
            {
                totalWeight += floor.getAngularPower(angle + (float)floor.floorNum * Mathf.PI * 0.5f);
            }
            totalWeight *= deltaVelocityPerWeight * Time.deltaTime;
            angularVelocity += totalWeight;
            angularVelocity = Mathf.Sign(angularVelocity) * (Mathf.Max(Mathf.Abs(angularVelocity) - anglurVelocityDecrease * Time.deltaTime, 0));
            if (angularVelocity > maxAngluarVelocity) angularVelocity = maxAngluarVelocity;
            else if (angularVelocity < -maxAngluarVelocity) angularVelocity = -maxAngluarVelocity;
            
            foreach (AltarScript floor in floors)
            {
                float floorAngle = angle + (float)floor.floorNum * Mathf.PI * 0.5f;
                float oldAngle = floorAngle;
                floorAngle += (angularVelocity * Time.deltaTime);

                Vector2 dDist = new Vector2(radius * Mathf.Cos(floorAngle), radius * Mathf.Sin(floorAngle)) - new Vector2(radius * Mathf.Cos(oldAngle), radius * Mathf.Sin(oldAngle));
                Rigidbody2D body = floor.GetComponent<Rigidbody2D>();
                if (Time.deltaTime > 0)
                    body.velocity = new Vector2(dDist.x / Time.deltaTime, dDist.y / Time.deltaTime);
                else
                    body.velocity = new Vector2(0, 0);
            }
            angle += (angularVelocity * Time.deltaTime);
        }
    }

    public void activatePlayers()
    {
        foreach (AltarScript floor in floors)
        {
            floor.activatePlayer();
        }
        playerActivated = true;
    }
}
