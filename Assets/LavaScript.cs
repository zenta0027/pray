﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LavaScript : MonoBehaviour
{

    public float maxHeight;
    public float lavaSpeed;
    public float bubbleRegenTime = 1;
    public int bubbleAmount = 6;
    bool playerActivated = false;
    float time = 0.0f;
    float maxWidth;
    //GameObject[] lavaBubbles;
    GameObject lavaBubble;
    GameObject lavaBorder;
    //int index = 0;

    // Use this for initialization
    void Start()
    {
        //lavaBubbles = new GameObject[3];
        //lavaBubbles[0] = transform.Find("lavaBubble1").gameObject;
        //lavaBubbles[1] = transform.Find("lavaBubble2").gameObject;
        //lavaBubbles[2] = transform.Find("lavaBubble3").gameObject;
        lavaBubble = transform.Find("lavaBubble1").gameObject;
        lavaBorder = transform.Find("lavaBorderTiling").gameObject;
        maxWidth = lavaBorder.GetComponent<SpriteRenderer>().sprite.bounds.max.x * lavaBorder.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        if(playerActivated)
        {
            if (transform.position.y < maxHeight)
            {
                gameObject.transform.position += new Vector3(0.0f, lavaSpeed * Time.deltaTime, 0.0f);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, maxHeight, transform.position.z);
            }
        }
        time += Time.deltaTime;
        if(time >= bubbleRegenTime)
        {
            time -= bubbleRegenTime;
            for(int i = 0; i < bubbleAmount; i++)
            {
                //GameObject child = Instantiate(lavaBubbles[index++]);
                GameObject child = Instantiate(lavaBubble);
                child.SetActive(true);
                child.transform.parent = transform;
                child.transform.position = new Vector3(Random.Range(-maxWidth, maxWidth), lavaBubble.transform.position.y, 0.0f);
                //index %= 3;
            }
        }
    }

    public void activateLava()
    {
        playerActivated = true;
    }
}
