﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MenuController : MonoBehaviour
{
    bool playerActivated = false;
    bool gameOver = false;
    GameObject countText;
    List<AltarScript> floors;
    public GameObject Explosion;

    // Use this for initialization
    void Start()
    {
        floors = new List<AltarScript>();
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            AltarScript floorScript = gameObject.transform.GetChild(i).GetComponent<AltarScript>();
            floors.Add(floorScript);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerActivated)
            activatePlayers();

        foreach (AltarScript floor in floors)
        {
            float readyAxis = Input.GetAxisRaw("P" + floor.floorNum + "_Ready");
            if (readyAxis > 0.5f && floor.gameObject.activeSelf)
            {
                GameObject owner = floor.getOwner();
                PlayerController playerScript = owner.GetComponent<PlayerController>();
                if (playerScript.isAlive())
                {
                    playerScript.createDeadEffect(1.0f);

                    GameObject explosionSprite = Instantiate(Explosion);
                    explosionSprite.transform.position = owner.transform.position;
                    explosionSprite.SetActive(true);

                    playerScript.kill(false);
                }

                GameObject altarSprite = Instantiate(floor.gameObject);
                altarSprite.transform.position = floor.gameObject.transform.position;
                altarSprite.GetComponent<BoxCollider2D>().enabled = false;
                altarSprite.AddComponent<FadeOut>();
                altarSprite.GetComponent<FadeOut>().fadeTime = 1.0f;

                for (int i = 1; i < floor.getAlterLightOn(); i++)
                {
                    GameObject alterLight = altarSprite.transform.Find("altarLight_" + floor.floorNum + "_" + i).gameObject;
                    alterLight.AddComponent<FadeOut>();
                    alterLight.GetComponent<FadeOut>().fadeTime = 1.0f;
                }
                floor.enabled = false;
                floor.gameObject.SetActive(false);
            }
        }

        bool allReadied = true;
        foreach (AltarScript floor in floors)
        {
            if(floor.gameObject.activeSelf)
                allReadied = false;
        }
        if (allReadied)
            Application.LoadLevel("LocalPlayScene");
    }

    public void activatePlayers()
    {
        foreach (AltarScript floor in floors)
        {
            floor.activatePlayer();
        }
        playerActivated = true;
    }
}
