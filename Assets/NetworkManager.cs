﻿using UnityEngine;
using Lidgren.Network;
using System;
using System.Xml.Serialization;
using System.IO;

public class NetworkManager : MonoBehaviour
{
    public class SyncMessage
    {
        public Vector3[] PlayerPos;
        public Vector3[] FloorPos;

        public SyncMessage()
        {
            PlayerPos = new Vector3[4];
            FloorPos = new Vector3[4];
        }
    }

    XmlSerializer xs;
    NetServer server;
    NetClient client;

    public String HostIP;
    public bool IsHost;

    public GameObject PlayerCharacter;
    public GameObject[] EnemyList;

    public bool IsNetworkConnected
    {
        get
        {
            if (IsHost == true)
            {
                if (server == null)
                    return false;
                // **TEMP**
                // reutrn client.ConnectionsCount == 3;
                return server.ConnectionsCount == 1;
            }
            else
            {
                if (client == null)
                    return false;
                return client.ConnectionStatus != NetConnectionStatus.None;
            }
        }
    }

    private void Awake()
    {
        xs = new XmlSerializer(typeof(SyncMessage));
    }
    private void Start()
    {
        DontDestroyOnLoad(transform);

        if (IsHost == true)
        {
            NetPeerConfiguration config = new NetPeerConfiguration("PRAY");
            config.MaximumConnections = 10;
            config.Port = 25252;
            server = new NetServer(config);
            server.Start();
        }
        else
        {
            NetPeerConfiguration config = new NetPeerConfiguration("PRAY");
            config.Port = 25252;
            client = new NetClient(config);
            client.Start();
            client.Connect(HostIP, 25252, client.CreateMessage("HANDSHAKING"));
        }
    }

    private void Update()
    {
        Debug.Log(IsNetworkConnected);
        if (IsNetworkConnected == true)
        {
            if (IsHost == true)
            {
                SyncMessage syncmsg = new SyncMessage();
                foreach (PlayerController pc in GameObject.FindObjectsOfType<PlayerController>())
                    syncmsg.PlayerPos[pc.playerNum - 1] = pc.transform.position;
                foreach (AltarScript fs in GameObject.FindObjectsOfType<AltarScript>())
                    syncmsg.PlayerPos[fs.floorNum - 1] = fs.transform.position;

                NetOutgoingMessage msg = server.CreateMessage();
                using (MemoryStream ms = new MemoryStream())
                {
                    xs.Serialize(ms, syncmsg);
                    ms.Position = 0;
                    using (StreamReader sr = new StreamReader(ms))
                        msg.Write(sr.ReadToEnd());
                }

                foreach (NetConnection nc in server.Connections)
                    nc.SendMessage(msg, NetDeliveryMethod.ReliableOrdered, 0);
                Debug.Log("Message sent");
            }
            else
            {
                while (true)
                {
                    NetIncomingMessage msg = client.ReadMessage();
                    if (msg == null)
                        break;

                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.Data:
                            {
                                SyncMessage syncmsg = null;
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    using (StreamWriter sw = new StreamWriter(ms))
                                    {
                                        sw.Write(msg.ReadString());
                                        ms.Position = 0;

                                        syncmsg = (SyncMessage)xs.Deserialize(ms);
                                    }
                                }
                                
                                foreach (PlayerController pc in GameObject.FindObjectsOfType<PlayerController>())
                                    pc.transform.position = syncmsg.PlayerPos[pc.playerNum - 1];
                                foreach (AltarScript fs in GameObject.FindObjectsOfType<AltarScript>())
                                    fs.transform.position = syncmsg.PlayerPos[fs.floorNum - 1];
                            }
                            break;

                        default:
                            Debug.Log(String.Format("Received {0}-typed message", msg.MessageType));
                            break;
                    }
                }
            }
        }
    }

    private void ResetConnection()
    {
        if (server != null)
        {
            foreach (NetConnection nc in server.Connections)
                nc.Disconnect("DISCONNECTED BY SERVER");
            server = null;
        }
        if (client != null)
        {
            client.Disconnect("DISCONNECTED BY CLIENT");
            client = null;
        }
    }
}
